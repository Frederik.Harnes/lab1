package rockPaperScissors;

import java.util.Arrays;
import java.util.List;
import java.util.Scanner;
import java.util.Random;

public class RockPaperScissors {
	
	public static void main(String[] args) {
    	/* 	
    	 * The code here does two things:
    	 * It first creates a new RockPaperScissors -object with the
    	 * code `new RockPaperScissors()`. Then it calls the `run()`
    	 * method on the newly created object.
         */
        new RockPaperScissors().run();
    }
    
    
    Scanner sc = new Scanner(System.in);
    int roundCounter = 1;
    int humanScore = 0;
    int computerScore = 0;
    List<String> rpsChoices = Arrays.asList("rock", "paper", "scissors");
    
    public void run() {
    while (true){
    
        System.out.println("Let's play round " + roundCounter);
        
        Random r = new Random();
        String random = rpsChoices.get(r.nextInt(rpsChoices.size())); 
       
        String human_choice = readInput("Your choice (Rock/Paper/Scissors)?");
        String choices = ("Human chose " + human_choice + ", computer chose " + random);
        while (rpsChoices.contains(human_choice) != true){
            human_choice = readInput("I do not understand " + human_choice +". Could you try again?");
        }  
            if (human_choice.equals("paper") && random.equals("rock")){
                humanScore ++;
                System.out.println(choices + ". Human wins!");
                    }
            else if (human_choice.equals("paper") && random.equals("scissors")){
                computerScore ++;
                System.out.println(choices + ". Computer wins!");
                            }   
            else if (human_choice.equals("rock") && random.equals("scissors")){
                humanScore ++;
                System.out.println(choices + ". Human wins!"); 
            }
            else if (human_choice.equals("rock") && random.equals("paper")){
                computerScore ++;
                System.out.println(choices + ". Computer wins!"); 
                }
            else if (human_choice.equals("scissors") && random.equals("paper")){
                humanScore ++;
                System.out.println(choices + ". Human wins!"); 
                    }        
            else if (human_choice.equals("scissors") && random.equals("rock")){
                computerScore ++;
                System.out.println(choices + ". Computer wins!"); 
                        }
            else if (human_choice.equals(random)){
                System.out.println(choices + ". It's a tie!"); 
            }

                System.out.println("Score: human " + humanScore + ", computer " + computerScore);
                roundCounter ++;
                String playAgain = readInput("Do you wish to continue playing? (y/n)?");
                if (playAgain.equals("n")){
                System.out.println("Bye bye :)");
                break;
                }
            }
        
}  

    /**
     * Reads input from console with given prompt
     * @param prompt
     * @return string input answer from user
     */
    public String readInput(String prompt) {
        System.out.println(prompt);
        String userInput = sc.next();
        return userInput;
    }

}


